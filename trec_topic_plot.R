options(warn=1)
argv <- commandArgs(TRUE)

# tolerance for floating point comparisons
tol = 1e-6

usage <- "Usage:
    %s  scores out.pdf

    Where scores is a dataframe with three columns: 
    - run_id
    - query_id
    - scores

    Options:
    --lib PATH
        Load particular libraries from path
    (--queries | -q) QUERIES:
        Show query strings instead of query ids as labels.
        In addition: also include queries that do not occur in
        'scores' in the plot. If queries occur in 'scores' that
        do not occur in 'QUERIES' then give an error.
        QUERIES should have two columns:
        - query_id
        - query_string
    (--title | -t) TITLE
    (--ylab | -y) YLAB
"

args <- character(0) 
options <- list("help" = FALSE, "queries" = "",
        "title" = "", "ylab" = "", "lib" = "")
state <- "parse"

for (arg in argv)
{
    if (state == "store_next") {
        options[[store_in]] <- arg
        state <- "parse"
    } else if (state == "append_next") {
        options[[append_to]] <- append(options[[append_to]], arg)
        state <- "parse"
    } else if (arg == "--help") {
        options[["help"]] <- TRUE
    } else if (arg == "--queries" || arg == "-q" ) {
        state <- "store_next"
        store_in <- "queries"
    } else if (arg == "--title" || arg == "-t" ) {
        state <- "store_next"
        store_in <- "title"
    } else if (arg == "--ylab" || arg == "-y" ) {
        state <- "store_next"
        store_in <- "ylab"
    } else if (arg == "--lib") {
        state <- "store_next"
        store_in <- "lib"
    } else {
        # it's a positional argument
        args <- append(args, arg)
    }
}

if (options[["help"]]) {
    write(usage, stderr())
    quit(status=0)
}

if (length(args) < 2) {
    write("Wrong number of arguments\n", stderr())
    write(sprintf(usage, "trec_topic_plot.R"), stderr())
    quit(status=2)
}

if (options[["lib"]] == "") {
    library(plyr)
} else {
    library(plyr, lib.loc=options[["lib"]])
}

df_scores <- args[1]
out.pdf <- args[2]


scores <- read.table(df_scores, header=FALSE, sep="\t")
names(scores) <- c("runid", "qid", "score")
scores$runid <- factor(scores$runid)

max_scores_per_qid <- ddply(
        scores,
        c("qid"),
        function (df) {
            data.frame(
                    score.max=max(df$score))
        })
max_scores_per_qid <- max_scores_per_qid[
        order(max_scores_per_qid$score.max, decreasing=TRUE),]

# order scores$runid by average score over topics.
avg_scores_per_runid <- ddply(
        scores,
        c("runid"),
        function (df) {
            data.frame(
                    score.mean=mean(df$score))
        })
avg_scores_per_runid <- avg_scores_per_runid[
        order(avg_scores_per_runid$score.mean, decreasing=TRUE),]
scores$runid <- factor(scores$runid, ordered=TRUE,
        levels=avg_scores_per_runid$runid)


# ylim
ymax <- max(max_scores_per_qid$score.max)
ymax <- ymax + 0.1 * ymax
ylim <- range(0, ymax)

# max length of qids / queries, to determine margin needed.
if (options[["queries"]] == "") {
    labels <- max_scores_per_qid$qid
} else {
    # use query file to obtain all qids, and query_strings
    queries <- read.table(options[["queries"]], header=FALSE, sep="\t")
    names(queries) <- c("qid", "query_string")
    # merge max_scores_per_qid with queries
    max_scores_per_qid <- merge(max_scores_per_qid,
            queries, by=c("qid"), all=TRUE)
    # if NA's in score.max: some queries were not in the run(s): set these to
    # zero.
    max_scores_per_qid$score.max[is.na(max_scores_per_qid$score.max)] <- 0.0
    # if NA's in query_string, some queries in runs were not in queries: give
    # an error and exit.
    if (length(which(is.na(max_scores_per_qid$query_string))) > 0) {
        write("trec_topic_plot.R: ERROR: \
Results in run(s) for non-existent queries", stderr()) 
        quit(status=3)
    }
    # order is not preserved by merge: reorder.
    max_scores_per_qid <- max_scores_per_qid[
            order(max_scores_per_qid$score.max, decreasing=TRUE),]
    labels <- max_scores_per_qid$query_string 
}
max_length_labels_inch <- max(strwidth(labels, "inch"))
max_height_labels_inch <- max(strheight(labels, "inch"))
bottommargin <- max_length_labels_inch
plot_area_width <- nrow(max_scores_per_qid) * 1.5 * max_height_labels_inch

# max length of runids, to determine width legend
max_length_runids <- max(strwidth(levels(scores$runid), "inch"))
legend_width <- max_length_runids + 1.0

# start the graph
figure_width=1.02 + plot_area_width + 0.42 + legend_width
pdf(file=out.pdf, width=figure_width)

par(mai=c(bottommargin + 1.02, 0.82, 0.82, 0.42 + legend_width))

# las = 3 means always vertical. Will need some margin for that.
mids <- barplot(max_scores_per_qid$score.max, names.arg=labels,
        ylab=options[["ylab"]], main=options[["title"]], xlab="",
        ylim=ylim, las=3)

mids_by_x <- mids[order(mids, decreasing=TRUE)]
legend_placement_x <- mids_by_x[1] + (mids_by_x[1] - mids_by_x[2])
legend_placement_y <- ymax

# now for each runid, put in points, with different pchs.
pch <- 1
for (runid in levels(scores$runid)) {
    X <- scores[scores$runid == runid, c("qid","score")]

    M <- merge(
            max_scores_per_qid[,
                    c("qid","score.max")],
            X,
            all.x=TRUE)
    M[is.na(M)] <- 0.0
    M <- M[order(M$score.max, decreasing=TRUE),]

    points(mids, M$score, pch=pch)
    pch <- pch + 1
}

abline(h=seq(from=0, to=ymax, by=0.2), lty=2, col='lightgrey')

legend(x=legend_placement_x,
        y=legend_placement_y,
        legend=levels(scores$runid),
        pch=1:length(levels(scores$runid)),
        xpd=TRUE)

device <- dev.off()
