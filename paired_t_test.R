options(warn=1)
library(plyr)
argv <- commandArgs(TRUE)

# tolerance for floating point comparisons
tol = 1e-6

usage <- "Usage:
    %s  scores 

    Where scores is a dataframe with three columns, the first being
    a condition, the second containing the condition you would like
    to pair observations on, and the third one containing measurements.

    Will compute a paired sample t-test for all possible pairs of
    conditions from the first column.
"

args <- character(0) 
options <- list("help" = FALSE, "params" = character(0), 
        "names" = character(0), "scores" = character(0))
state <- "parse"

for (arg in argv)
{
    if (state == "store_next") {
        options[[store_in]] <- arg
        state <- "parse"
    } else {
        if (state == "append_next") {
            options[[append_to]] <- append(options[[append_to]],
                    arg)
            state <- "parse"
        } else {
            if (arg == "--help") {
                options[["help"]] <- TRUE
            } else {
                if (arg == "--some-option-with-value") {
                    state <- "store_next"
                    store_in <- "some_option_with_value"
                } else {
                    # it's a positional argument
                    args <- append(args, arg)
                }
            }
        }
    }
}

if (options[["help"]]) {
    write(usage, stderr())
    quit(status=0)
}

if (length(args) != 1) {
    write("Wrong number of arguments\n", stderr())
    write(sprintf(usage, "paired_t_test.R"), stderr())
    quit(status=2)
}

df <- read.table(args[1], header=FALSE, sep="\t")
names(df) <- c("condition", "pair_condition", "x")
df$condition <- factor(df$condition)
df$pair_condition <- factor(df$pair_condition)

# the distribution of differences per pair should be normal
# we are not going to check that here.

condition_pairs <- combn(levels(df$condition), 2, simplify=FALSE)

for (p in condition_pairs) {
    X <- df[df$condition == p[1], c("pair_condition", "x")]
    Y <- df[df$condition == p[2], c("pair_condition", "x")]

    # now we check that there is one and only one observation in both X
    # and Y for each value of pair_condition.
    # First check that the lengths of X and Y are ok.
    if (length(levels(df$pair_condition)) != length(X$pair_condition)) { quit(status=1) }
    if (length(levels(df$pair_condition)) != length(Y$pair_condition)) { quit(status=1) }
    # Then check if X and Y contain the same elements.
    if (!setequal(X$pair_condition, Y$pair_condition)) { quit(status=1) }

    X <- X[order(X$pair_condition), c("x"), drop=TRUE]
    Y <- Y[order(Y$pair_condition), c("x"), drop=TRUE]
    t <- t.test(X, Y, paired=TRUE) # does a paired t-test 

    cat(sprintf("%s\t%s\t%.6f\t%.6f\t%.6f\n", p[1], p[2], mean(X),
            mean(Y), t$p.value))
}
